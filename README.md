Project's code for [Winter Workshop on Complex Systems 2015](http://ai.vub.ac.be/complexity/)
See the initial project proposal [here](https://bitbucket.org/vveitas/p-sense-making/downloads/project_proposal.pdf)
